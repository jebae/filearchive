//
// Created by 배정모 on 2017. 11. 28..
//
#include "archive.h"
#define ARCHIVE_NAME "archive.bin"

PARCHIVE initialize(){
    PARCHIVE archive = malloc(sizeof(ARCHIVE));
    memset(archive, 0, sizeof(ARCHIVE));
    FILE *fp = fopen(ARCHIVE_NAME, "r+b");

    if (fp == NULL) {
        fp = fopen(ARCHIVE_NAME, "w+b");
        if (fp == NULL)
        {
            printf ("Cannot open file %s", ARCHIVE_NAME);
            goto ERROR1;
        }

        archive->header.magic = 'AF';
        archive->header.version = 1;

        if (fwrite(&archive->header, sizeof(ARCHIVE_HEADER), 1, fp) < 1)
        {
            printf ("Fail to write archive header to file \"%s\"\n", ARCHIVE_NAME);
            goto ERROR2;
        }
    } else {
        if (fread(&archive->header, sizeof(ARCHIVE_HEADER), 1, fp) < 1) {
            printf("No archive header\n");
            goto ERROR2;
        }
    }

    if (archive->header.magic != 'AF'){
        printf ("Magic number is not correct\n");
        goto ERROR2;
    }

    if (archive->header.version != 1){
        printf ("Version is not suitable\n");
        goto ERROR2;
    }

    archive->fp = fp;

    uint32_t size;
    uint32_t currPos = ftell(fp);

    size = getFileSize(fp);
    while (size > currPos) {
        PFILE_NODE node = malloc(sizeof(FILE_NODE));
        fread(&node->desc, sizeof(FILE_DESC), 1, fp);
        node->next = archive->filelist.next;
        archive->filelist.next = node;

        fseek(fp, node->desc.size, SEEK_CUR);
        currPos = ftell(fp);
    }
    return archive;

ERROR2:
    fclose(fp);
ERROR1:
    free(archive);
}

void finalize(PARCHIVE archive){

    PFILE_NODE cur = archive->filelist.next;
    while (cur != NULL) {
        PFILE_NODE next = cur->next;
        free(cur);
        cur = next;
    }
    fclose(archive->fp);
    free(archive);
}

bool isExist(PARCHIVE archive, char *filename){
    PFILE_NODE cur = archive->filelist.next;

    while (cur != NULL){
        if (strcmp(filename, cur->desc.name) == 0)
            return true;
        cur = cur->next;
    }
    return false;
}