//
// Created by 배정모 on 2017. 11. 27..
//
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <memory.h>
#include <stdbool.h>
#include "file.h"

#ifndef ARCHIVE_ARCHIVE_H
#define ARCHIVE_ARCHIVE_H

#pragma pack(push, 1)
typedef struct _ARCHIVE_HEADER{
    uint16_t magic;
    uint16_t version;
} ARCHIVE_HEADER, *PARCHIVE_HEADER;

#pragma pack(pop)

typedef struct _ARCHIVE {
    ARCHIVE_HEADER header;
    FILE *fp;
    FILE_NODE filelist;
} ARCHIVE, *PARCHIVE;

PARCHIVE initialize();
void finalize(PARCHIVE archive);
bool isExist(PARCHIVE archive, char *filename);
#endif //ARCHIVE_ARCHIVE_H
