//
// Created by 배정모 on 2017. 11. 27..
//

#include "command.h"

int append(PARCHIVE archive, char *filename)
{
    int ret = 0;
    FILE *fp = fopen(filename, "rb");
    if (fp == NULL)
    {
        printf ("Cannot open file %s\n", filename);
        return -1;
    }

    uint8_t *buffer;
    uint32_t size;
    size = getFileSize(fp);
    buffer = malloc(size);

    if (fread(buffer, size, 1, fp) < 1)
    {
        ret = -1;
        goto ERROR1;
    }

    PFILE_DESC desc = malloc(sizeof(FILE_DESC));
    memset(desc, 0, sizeof(FILE_DESC));
    strcpy(desc->name, filename);
    desc->size = size;

    PFILE_NODE node = archive->filelist.next;
    if (node == NULL)
        fseek(archive->fp, sizeof(ARCHIVE_HEADER), SEEK_SET);
    else
        fseek(archive->fp, node->desc.dataOffset + node->desc.size, SEEK_SET);
    desc->dataOffset = ftell(archive->fp) + sizeof(FILE_DESC);

    if (fwrite(desc, sizeof(FILE_DESC), 1, archive->fp) < 1)
    {
        printf ("Fail to write file description\n");
        ret = -1;
        goto ERROR2;
    }

    if (fwrite(buffer, size, 1, archive->fp) < 1)
    {
        printf ("Fail to write file data\n");
        ret = -1;
        goto ERROR2;
    }

    printf ("Success to archive file \"%s\" | size: %lld\n", filename, size);

ERROR2:
    free(desc);

ERROR1:
    free(buffer);
    fclose(fp);
    return ret;
}

int list(PARCHIVE archive) {
    PFILE_NODE cur = archive->filelist.next;
    printf ("File list:\n");
    while (cur != NULL) {
        printf ("\t-%s\n", cur->desc.name);
        cur = cur->next;
    }
    return 0;
}

int contents(PARCHIVE archive) {
    uint32_t size;
    PFILE_NODE fileNode = archive->filelist.next;

    printf ("\nFile contents:\n");
    while (fileNode != NULL) {
        size = fileNode->desc.size;
        uint8_t *buffer = malloc(size + 1);
        memset(buffer, 0, size + 1);
        fseek(archive->fp, fileNode->desc.dataOffset, SEEK_SET);
        fread(buffer, size, 1, archive->fp);
        printf ("\t-%d %s\n", size, buffer);
        free(buffer);
        fileNode = fileNode->next;
    }
    return 0;
}

int extract(PARCHIVE archive, char *filename){
    PFILE_NODE cur = archive->filelist.next;
    char copyright[] = "Copyright by flyingbingbong";
    while (cur != NULL){
        if (strcmp(cur->desc.name, filename) == 0){
            FILE *fp = fopen(filename, "wb");
            if (fp == NULL){
                printf ("Cannot open file %s\n", filename);
                return -1;
            }

            uint32_t size = cur->desc.size;
            uint8_t *buffer = malloc(size + strlen(copyright));
            memset(buffer, 0, size + strlen(copyright));
            fseek(archive->fp, cur->desc.dataOffset, SEEK_SET);
            if (fread(buffer, size, 1, archive->fp) < 1){
                printf ("Cannot read to buffer from archive file data");
                goto ERROR1;
            }
            strcat(buffer, copyright);
            if (fwrite(buffer, size + strlen(copyright), 1, fp) < 1){
                printf ("Cannot write buffer to file %s\n", filename);
                goto ERROR1;
            }
            printf ("Success to extract file \"%s\" from archive (size: %llu)\n", filename, size);

        ERROR1:
            free(buffer);
            fclose(fp);
            return 0;
        }
        cur = cur->next;
    }
    return -1;
}