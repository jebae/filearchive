//
// Created by 배정모 on 2017. 11. 27..
//

#ifndef ARCHIVE_COMMAND_H
#define ARCHIVE_COMMAND_H

#include "archive.h"

int append(PARCHIVE archive, char *filename);
int list(PARCHIVE archive);
int contents(PARCHIVE archive);
int extract(PARCHIVE archive, char *filename);

#endif //ARCHIVE_COMMAND_H
