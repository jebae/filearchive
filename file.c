//
// Created by 배정모 on 2017. 11. 27..
//
#include "file.h"

uint32_t getFileSize(FILE *fp)
{
    uint32_t size;
    uint32_t currPos = ftell(fp);

    fseek(fp, 0, SEEK_END);
    size = ftell(fp);
    fseek(fp, currPos, SEEK_SET);
    return size;
}