//
// Created by 배정모 on 2017. 11. 27..
//

#ifndef ARCHIVE_FILE_H
#define ARCHIVE_FILE_H
#include <stdio.h>
#include <stdint.h>

#pragma pack(push, 1)
typedef struct _FILE_DESC {
    char name[256];
    uint32_t size;
    uint32_t dataOffset;
} FILE_DESC, *PFILE_DESC;
#pragma pack(pop)

typedef struct _FILE_NODE{
    FILE_DESC desc;
    struct _FILE_NODE *next;
} FILE_NODE, *PFILE_NODE;

uint32_t getFileSize(FILE *fp);

#endif //ARCHIVE_FILE_H
