#include "command.h"

int main(int argc, char *argv[]) {
    if (argc == 1){
        printf("filearchive <command> <filename>\n\n");
        printf ("Command list:\n");
        printf ("\tappend =>\tappend file\n");
        printf ("\tlist =>\tlist files\n");
        printf ("\textract =>\textract file from archive\n");
        return 0;
    }

    PARCHIVE archive = initialize();
    if (archive == NULL)
        return -1;
    char command = argv[1][0];
    char *filename= argv[2];

    switch (command){
        case 'a':
            if (!isExist(archive, filename)){
                if (append(archive, filename) == 0)
                    printf ("Success to append\n");
                else
                    printf ("Fail to append\n");
            }
            else
                printf ("Already existed file\n");
            break;
        case 'l':
            list(archive);
            break;
        case 'e':
            if (isExist(archive, filename)){
                if (extract(archive, filename) == -1)
                    printf ("Fail to extract file \"%s\"\n", filename);
                else
                    printf ("Success to extract\n");
            }
            else
                printf ("No file name like \"%s\"\n", filename);
            break;
        default:
            break;
    }
    finalize(archive);
    return 0;
}